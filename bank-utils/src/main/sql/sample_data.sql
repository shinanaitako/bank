DELETE FROM role;

INSERT INTO role(name) VALUES('bank.user');
INSERT INTO role(name) VALUES('bank.admin');

DELETE FROM users;

INSERT INTO users(name, email, username, password) VALUES('Elise Mõttus', 'elisemottus@gmail.com', 'elise', '3f122fd6ff97ebdcef91e2e1af20b136397fc6442af86be4ebc20114eab8fa91');
INSERT INTO users(name, email, username, password) VALUES('Administrator', 'admin@digipurk.ee', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');